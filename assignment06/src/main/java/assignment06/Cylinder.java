/*Noah Gelinas */
package assignment06;
     
public class Cylinder implements Shape3d{
    private double radius=0;
    private double height=0;
    
    public Cylinder (double radius,double height) {
        this.radius=radius;
        this.height=height;
    }
    /**
     * returns volume of cylinder
     * @return the volume
     */
    public double getVolume() {
        return Math.PI*Math.pow(this.radius,2)*this.height;
    }

     /**
     * returns surface area of cylinder
     * @return the surface area
     */
    public double getSurfaceArea() {
        return (2*Math.PI)*Math.pow(this.radius,2)+(2*Math.PI*this.radius*this.height);
    }
}

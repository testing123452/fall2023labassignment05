/* Jaypee Tello */

package assignment06;

public class Sphere implements Shape3d {
  private double radius;

  public Sphere(double radius) {
    this.radius = radius;
  }

  @Override
  /**
   * Gets the volume of a sphere
   * 
   * @return The volume of the sphere
   */
  public double getVolume() {
    return (4 / 3 * Math.PI * Math.pow(this.radius, 3));
  }

  @Override
  /**
   * Gets the surface area of a sphere
   * 
   * @return The surface area of the sphere
   */
  public double getSurfaceArea() {
    return (4 * Math.PI * Math.pow(this.radius, 2));
  }
}

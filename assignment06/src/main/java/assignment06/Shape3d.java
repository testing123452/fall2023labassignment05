package assignment06;

public interface Shape3d {
  double getVolume();

  double getSurfaceArea();
}
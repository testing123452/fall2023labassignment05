/*Noah Gelinas */
package assignment06;

import static org.junit.Assert.*;

import org.junit.Test;

public class SphereTest {
    @Test
    public void shouldWorkCuzNoConstructorDefined() {
        Shape3d s = new Sphere(3);
    }

    @Test
    public void getVolume() {
        Shape3d s = new Sphere(3);
        s.getVolume();
    }

    @Test
    public void getSurfaceArea() {
        Shape3d s = new Sphere(3);
        s.getSurfaceArea();
    }
}

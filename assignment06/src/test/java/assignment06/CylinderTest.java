package assignment06;

/* Jaypee Tello */
import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
  @Test
  public void testGetVolume() {
    Shape3d c = new Cylinder(3,4);
    c.getVolume();
    
  }

  @Test
  public void testGetSurfaceArea() {
    Shape3d c = new Cylinder(3,4);
    c.getSurfaceArea();
    
  }

  @Test
  public void testConstructor() {
    Shape3d c = new Cylinder(3,4);
  }
}
